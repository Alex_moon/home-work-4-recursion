func createResultArray ( _ number: Int) -> [Int:[Int]] {
    var myArray = [number]
    func getFactorNumbers (_ n: Int, _ k: Int, _ mas: inout [Int]) {
    if (n - 1) == 1 {
         mas.append(n-1)
    } else {
            if k % (n-1) == 0 {
                mas.append(n-1)
            }
            getFactorNumbers((n-1), k, &mas)
        }
    }
    getFactorNumbers(number, number, &myArray)
    return [myArray[0]: myArray]
}

print(createResultArray(531))
func getTuple (_ array: [Int]) -> (Int, Int, Int) {
    var min = array[0]
    var max = array[0]
    var avg = array[0]
    func getMinMaxAvg ( _ array: [Int], _ index: Int, _ min: inout Int, _ max: inout Int, _ avg: inout Int) -> (Int, Int, Int) {
        if index == array.count {
            avg = avg / array.count
            return (min, max, avg)
        } else {
            if array[index] < min {
            min = array[index]
            }
        if array[index] > max {
            max = array[index]
        }
        avg += array[index]
        return getMinMaxAvg(array, index + 1, &min, &max, &avg)
        }
    }
    let resultTuple = getMinMaxAvg(array, 1, &min, &max, &avg)
    return resultTuple
}

func printResult (_ tuple: (Int, Int, Int)) {
    print ("min = \(tuple.0), max = \(tuple.1), average = \(tuple.2)")    
}

var exampleArray = [10, 15, 20, 25, 5, 30]

printResult(getTuple(exampleArray))